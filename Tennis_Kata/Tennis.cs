﻿using System;
using System.Collections.Generic;

namespace Tennis_Kata
{
    public class Tennis
    {
        private int _firstPlayerScore;

        private Dictionary<int, string> _scoreLookUp = new Dictionary<int, string>()
        {
            {0,"Love" },
            {1,"Fifteen" },
            {2,"Thirty" },
            {3,"Forty" },
        };

        private int _secondPlayerScore;

        public string GetScore()
        {
            if (IsSameScore())
            {
                return IsDeuce() ? "Deuce" : _scoreLookUp[_firstPlayerScore] + "_All";
            }

            if (IsGamePoint())
            {
                return string.Concat(AdvPlayer(), IsAdv() ? "_Adv" : "_Win")  ;
            }

            return _scoreLookUp[_firstPlayerScore] + "_" + _scoreLookUp[_secondPlayerScore];
        }

        private bool IsAdv()
        {
            return Math.Abs(_firstPlayerScore - _secondPlayerScore) == 1;
        }

        private bool IsGamePoint()
        {
            return _firstPlayerScore > 3 || _secondPlayerScore > 3;
        }

        private string AdvPlayer()
        {
            if (_firstPlayerScore > _secondPlayerScore)
            {
                return "FirstPlayer";
            }

            return "SecondPlayer";
        }

        private bool IsDeuce()
        {
            return _firstPlayerScore >= 3 && _secondPlayerScore >= 3;
        }

        private bool IsSameScore()
        {
            return _secondPlayerScore == _firstPlayerScore;
        }

        public void firstPlayerGotScore()
        {
            _firstPlayerScore++;
        }

        public void secondPlayerGotScore()
        {
            _secondPlayerScore++;
        }
    }
}