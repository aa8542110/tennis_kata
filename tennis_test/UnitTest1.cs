﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tennis_Kata;

namespace tennis_test
{
    [TestClass]
    public class UnitTest1
    {
        private Tennis _tennis = new Tennis();

        [TestMethod]
        public void Love_All()
        {
            ScoreShouldBe("Love_All");
        }

        [TestMethod]
        public void Fifteen_Love()
        {
            _tennis.firstPlayerGotScore();
            ScoreShouldBe("Fifteen_Love");
        }

        [TestMethod]
        public void Thirty_Love()
        {
            GivenFirstPlayerScore(2);
            ScoreShouldBe("Thirty_Love");
        }

        [TestMethod]
        public void Forty_Love()
        {
            GivenFirstPlayerScore(3);
            ScoreShouldBe("Forty_Love");
        }

        [TestMethod]
        public void Love_Fifteen()
        {
            _tennis.secondPlayerGotScore();
            ScoreShouldBe("Love_Fifteen");
        }

        [TestMethod]
        public void Love_Thirty()
        {
            GivenSecondPlayerScore(2);
            ScoreShouldBe("Love_Thirty");
        }

        [TestMethod]
        public void Fifteen_All()
        {
            _tennis.firstPlayerGotScore();
            _tennis.secondPlayerGotScore();
            ScoreShouldBe("Fifteen_All");
        }
        [TestMethod]
        public void Deuce_When_3VS3()
        {
            GivenSecondPlayerScore(3);
            GivenFirstPlayerScore(3);
            ScoreShouldBe("Deuce");
        }
        [TestMethod]
        public void Deuce_When_4VS4()
        {
            GivenSecondPlayerScore(4);
            GivenFirstPlayerScore(4);
            ScoreShouldBe("Deuce");
        }
        [TestMethod]
        public void FirstPlayer_Adv()
        {
            GivenSecondPlayerScore(3);
            GivenFirstPlayerScore(4);
            ScoreShouldBe("FirstPlayer_Adv");
        }
        [TestMethod]
        public void SecondPlayer_Adv()
        {
            GivenSecondPlayerScore(5);
            GivenFirstPlayerScore(4);
            ScoreShouldBe("SecondPlayer_Adv");
        }
        [TestMethod]
        public void FirstPlayer_Win_Wheb_6vs4()
        {
            GivenFirstPlayerScore(6);
            GivenSecondPlayerScore(4);
            ScoreShouldBe("FirstPlayer_Win");
        }
        [TestMethod]
        public void SecondPlayer_Win_Wheb_1vs4()
        {
            GivenFirstPlayerScore(1);
            GivenSecondPlayerScore(4);
            ScoreShouldBe("SecondPlayer_Win");
        }

        private void GivenFirstPlayerScore(int times)
        {
            for (int i = 0; i < times; i++)
            {
                _tennis.firstPlayerGotScore();
            }
        }

        private void GivenSecondPlayerScore(int times)
        {
            for (int i = 0; i < times; i++)
            {
                _tennis.secondPlayerGotScore();
            }
        }

        private void ScoreShouldBe(string socre)
        {
            Assert.AreEqual(socre, _tennis.GetScore());
        }
    }
}